package org.example.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class DemoBean {
    private String id;
    private String name;
    private String desc;
    private String code;
}
