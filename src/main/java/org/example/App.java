package org.example;

import org.example.model.DemoBean;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        DemoBean bean = DemoBean.builder().build();
        bean.setId("001");
        bean.setName("Demo-test1");
        bean.setDesc("Dummy Desc");
        bean.setCode("C#123");
        System.out.println(bean);
    }
}
